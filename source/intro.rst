.. SPDX-FileCopyrightText: 2017-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _intro:

Introduction
============

Files are written and used in netCDF format at Hereon in several institutes.
Files originating from Hereon should  be written in uniform nomenclature. This
promotes and facilitates exchange and dissemination. There are specifications
in regard to naming both attributes as well as variables (naming conventions).
In addition, there are specifications regarding which attributes should be
added to certain variables. Not all specifications are mandatory and, in
certain cases, adjustments are also necessary.

This information essentially refers to the agreements within the netCDF
Climate and Forecast (CF) Metadata Conventions [1]_ (hereafter referred to
as the CF Conventions). The relevant binding regulations are summarised and
expanded in the following document.

In addition to the CF Conventions, there are other agreements that are only
relevant to parts of Hereon. When dealing with observation data intended for
CMEMS [2]_, the rules, for example, for the OceanSITES Format Reference
[3]_ should be followed. These are not, however, used internally at Hereon.
In addition, the SeaDataNet netCDF conventions [4]_, which are themselves
based on the CF Conventions, may be relevant to other users.

After saving the data according to the specified regulations, the files should
be checked using the CF Conventions Compliance Checker for NetCDF Format
[5]_. In addition, a second person should review whether the special Hereon
regulations have been followed.

After successful checks, measurement data can be saved in the COSYNA data area
(contact: Gisbert Breitbach) and model data can, for example, be saved in the
DKRZ's CERA database with a DOI designation.

.. [1] https://cfconventions.org/index.html
.. [2] https://marine.copernicus.eu
.. [3] https://repository.oceanbestpractices.org/handle/11329/874.2
.. [4] https://www.seadatanet.org/content/download/637/3338/file/SDN_D85_WP8_netCDF_files_examples.zip
.. [5] https://compliance.ioos.us/index.html 
