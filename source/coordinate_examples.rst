.. SPDX-FileCopyrightText: 2017-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _appendix:

Sample Collection on Coordinate Information
===========================================

.. _example-basic:

Example A1
----------
In this example, the variable ``WaterLevel`` has the dimensions ``time``,
``latitude`` and ``longitude``. Here the `coordinate` variables are ``time``,
``longitude`` and ``latitude``, with names that are identical to the dimension
names. That is, it is a three-dimensional grid and/or a time series of surfaces.

.. code-block::

    dimensions:
            time = UNLIMITED ; // (177 currently)
            longitude = 69 ;
            latitude = 64 ;
    variables:
        double time(time) ;
            time:units = "days since 1970-01-01 00:00:00" ;
            time:standard_name = "time" ;
        double longitude(longitude) ;
            longitude:units = "degrees_east" ;
            longitude:standard_name = "longitude" ;
        double latitude(latitude) ;
            latitude:units = "degrees_north" ;
            latitude:standard_name = "latitude" ;
        double WaterLevel(time, latitude, longitude) ;
            WaterLevel:units = "m" ;
            WaterLevel:standard_name="sea_surface_height_above_sea_level"

.. _example-trajectory:

Example A2
----------
The dataset contains a trajectory -- for example, glider data.

.. code-block::

    dimensions:
            time = UNLIMITED ; // (177 currently)
    variables:
        double time(time) ;
            time:units = "days since 1970-01-01 00:00:00" ;
            time:standard_name = "time" ;
        double longitude(time) ;
            longitude:units = "degrees_east" ;
            longitude:standard_name = "longitude" ;
        double latitude(time) ;
            latitude:units = "degrees_north" ;
            latitude:standard_name = "latitude" ;
        double Pressure (time) ;
            Pressure:units = "dbar" ;
            Pressure:positive = "down"
            Pressure:standard_name = "sea_water_pressure" ;
        double Salinity(time) ;
            Salinity:units = "1" ;
            Salinity:standard_name = "sea_water_salinity" ;

.. _cosyna-distribution-statement:

Example A3
----------
Example of a COSYNA Distribution Statement

    COSYNA provides free public access to observational and model data. For
    near-real time data only limited quality control is performed. Accessible
    data are subject to change due to delayed processing and quality control.
    For all COSYNA data no warranty is made, expressed or implied, regarding
    the accuracy or validity of the data, or regarding the suitability of the
    data for any particular application. Guidelines for data use and
    publication: the right of priority use is reserved. If you intend to use
    COSYNA data please consult the COSYNA coordinator. The following conditions
    apply to any use of COSYNA data. The user agrees to acknowledge the
    provision of the data referring to the data originator and to include the
    following statement: \"The data were provided by the COSYNA system operated
    by Helmholtz-Zentrum hereon GmbH\"; to offer co-authorship to the data
    originator(s) if the provided COSYNA data are a principal component of the
    publication; not to use any data commercially without prior approval of the
    data owner(s). In addition, the data policies of the respective third party
    institution(s) must be followed in regard to any type of third party data
    provided by COSYNA.

.. _example-cosmo-clm:

Example A4
----------
Output from COSMO-CLM

.. code-block::

    dimensions:
            time = UNLIMITED ; // (2919 currently)
            rlat = 170 ;
            rlon = 148 ;
            nb2 = 2 ;
    variables:
        float TOT_PREC(time, rlat, rlon) ;
            TOT_PREC:standard_name = "precipitation_amount" ;
            TOT_PREC:long_name = "total precipitation amount" ;
            TOT_PREC:units = "kg m-2" ;
            TOT_PREC:grid_mapping = "rotated_pole" ;
            TOT_PREC:coordinates = "lon lat" ;
            TOT_PREC:cell_methods = "time: sum" ;
        float lat(rlat, rlon) ;
            lat:standard_name = "latitude" ;
            lat:long_name = "latitude" ;
            lat:units = "degrees_north" ;
        float lon(rlat, rlon) ;
            lon:standard_name = "longitude" ;
            lon:long_name = "longitude" ;
            lon:units = "degrees_east" ;
        float rlat(rlat) ;
            rlat:standard_name = "grid_latitude" ;
            rlat:long_name = "rotated latitude" ;
            rlat:units = "degrees" ;
        float rlon(rlon) ;
            rlon:standard_name = "grid_longitude" ;
            rlon:long_name = "rotated longitude" ;
            rlon:units = "degrees" ;
        char rotated_pole ;
            rotated_pole:long_name = "coordinates of the rotated North Pole" ;
            rotated_pole:grid_mapping_name = "rotated_latitude_longitude" ;
            rotated_pole:grid_north_pole_latitude = 54.2f ;
            rotated_pole:grid_north_pole_longitude = -57.7f ;
        double time(time) ;
            time:standard_name = "time" ;
            time:long_name = "time" ;
            time:units = "seconds since 1948-01-01 00:00:00" ;
            time:calendar = "proleptic_gregorian" ;
            time:bounds = "time_bnds" ;
        double time_bnds(time, nb2) ;
            time_bnds:units = "seconds since 1948-01-01 00:00:00" ;

    // global attributes:
            :title = "COSMO-CLM simulation for Bohai, Yellow and East China Sea" ;
            :institution = "Helmholtz-Zentrum Hereon, Institute of Coastal Systems, Germany" ;
            :source = "cosmo_100614_4.14_clm2 int2lm_120824_1.20_clm1" ;
            :project_id = "-" ;
            :experiment_id = "BH_LONG_ERAin" ;
            :realization = 1 ;
            :Conventions = "CF-1.6" ;
            :conventionsURL = "http://cfconventions.org/" ;
            :contact = "http://coastmod.hereon.de" ;
            :references = "doi:10.1002/2015JD024177 and http://www.clm-community.eu/" ;
            :creation_date = "2013-09-13T18:45:45" ;
            :originator = "Delei Li" ;
            :crs = "WGS84" ;
            :geospatial_lon_min = 117.f ;
            :geospatial_lat_min = 30.7f ;
            :geospatial_lat_max = 41.3f ;
            :geospatial_lon_max = 128.f ;

.. _example-dwd-radiosonde:

Example A5
----------

.. code-block::

    netcdf DWD_Radiosondes_05839.20080101_20081231 {
        dimensions:
            obs = UNLIMITED ; // (957166 currently)
            profile = 774 ;
        variables:
            float profile(profile) ;
                profile:cf_role = "profile_id" ;
            int parentIndex(obs) ;
                parentIndex:long_name = "index of profile" ;
                parentIndex:instance_dimension = "profile" ;
            double time(obs) ;
                time:standard_name = "time" ;
                time:units = "hours since 1970-01-01 00:00:00" ;
            float lat(profile) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degree_north" ;
            float lon(profile) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degree_east" ;
            float alt(profile) ;
                alt:standard_name = "altitude" ;
                alt:long_name = "height above geoid" ;
                alt:units = "m" ;
                alt:coordinates = "lon lat" ;
            float PS(profile) ;
                PS:standard_name = "surface_air_pressure" ;
                PS:long_name = "ground air pressure" ;
                PS:units = "hPa" ;
                PS:coordinates = "startTime lon lat" ;
                PS:_FillValue = -1.e+20f ;
            float T_2M(profile) ;
                T_2M:standard_name = "air_temperature" ;
                T_2M:long_name = "ground air temperature" ;
                T_2M:units = "degree_C" ;
                T_2M:coordinates = "startTime lon lat" ;
                T_2M:_FillValue = -1.e+20f ;
            float RELHUM_2M(profile) ;
                RELHUM_2M:standard_name = "relative_humidity" ;
                RELHUM_2M:long_name = "ground relative humidity" ;
                RELHUM_2M:units = "%" ;
                RELHUM_2M:coordinates = "startTime lon lat" ;
                RELHUM_2M:_FillValue = -1.e+20f ;
            float reportingTime(profile) ;
                reportingTime:standard_name = "time" ;
                reportingTime:long_name = "reporting time" ;
                reportingTime:units = "hours since 1970-01-01 00:00:00" ;
                reportingTime:_FillValue = -1.e+20f ;
            float startTime(profile) ;
                startTime:standard_name = "time" ;
                startTime:long_name = "start of sounding" ;
                startTime:units = "hours since 1970-01-01 00:00:00" ;
                startTime:_FillValue = -1.e+20f ;
            float CAPE(profile) ;
                CAPE:standard_name = "atmosphere_convective_available_potential_energy" ;
                CAPE:long_name = "convective available potential energy" ;
                CAPE:units = "J kg-1" ;
                CAPE:coordinates = "reportingTime lon lat" ;
                CAPE:_FillValue = -1.e+20f ;
            float ae_lat(obs) ;
                ae_lat:standard_name = "latitude" ;
                ae_lat:long_name = "sounding latitude" ;
                ae_lat:units = "degree_north" ;
                ae_lat:coordinates = "time" ;
                ae_lat:_FillValue = -1.e+20f ;
            float ae_lon(obs) ;
                ae_lon:standard_name = "longitude" ;
                ae_lon:long_name = "sounding longitude" ;
                ae_lon:units = "degree_east" ;
                ae_lon:coordinates = "time" ;
                ae_lon:_FillValue = -1.e+20f ;
            float FIH(obs) ;
                FIH:standard_name = "geopotential_height" ;
                FIH:long_name = "geopotential height" ;
                FIH:units = "m" ;
                FIH:coordinates = "time ae_lon ae_lat" ;
                FIH:_FillValue = -1.e+20f ;
            float P(obs) ;
                P:standard_name = "air_pressure" ;
                P:long_name = "air pressure" ;
                P:units = "hPa" ;
                P:coordinates = "time ae_lon ae_lat" ;
                P:_FillValue = -1.e+20f ;
            float T(obs) ;
                T:standard_name = "air_temperature" ;
                T:long_name = "air temperature" ;
                T:units = "degree_C" ;
                T:coordinates = "time ae_lon ae_lat FIH" ;
                T:_FillValue = -1.e+20f ;
            float RELHUM(obs) ;
                RELHUM:standard_name = "relative_humidity" ;
                RELHUM:long_name = "relative humidity" ;
                RELHUM:units = "%" ;
                RELHUM:coordinates = "time ae_lon ae_lat FIH" ;
                RELHUM:_FillValue = -1.e+20f ;
            float TD(obs) ;
                TD:standard_name = "dew_point_temperature" ;
                TD:long_name = "dew point temperature" ;
                TD:units = "degree_C" ;
                TD:coordinates = "time ae_lon ae_lat FIH" ;
                TD:_FillValue = -1.e+20f ;
            float WSS(obs) ;
                WSS:standard_name = "wind_speed" ;
                WSS:long_name = "wind speed" ;
                WSS:units = "m s-1" ;
                WSS:coordinates = "time ae_lon ae_lat FIH" ;
                WSS:_FillValue = -1.e+20f ;
            float WDIR(obs) ;
                WDIR:standard_name = "wind_from_direction" ;
                WDIR:long_name = "wind direction" ;
                WDIR:units = "degree" ;
                WDIR:coordinates = "time ae_lon ae_lat FIH" ;
                WDIR:_FillValue = -1.e+20f ;

        // global attributes:
                :institution = "DWD" ;
                :station = "Emden" ;
                :station_id = "05839" ;
                :source = "measurements: radiosonde ascents" ;
                :instrument = "Radiosonde RS92-SGP" ;
                :instrument_type = "MSGRT" ;
                :featureType = "profile" ;
                :Conventions = "CF-1.7" ;
                :conventionsURL = "http://www.unidata.ucar.edu/packages/netcdf/conventions.html" ;
                :download_site = "ftp://opendata.dwd.de/climate_environment/CDC/observations_germany/radiosondes/high_resolution/historical" ;
                :licence = "Geodatennutzungsverordnung, GeoNutzV, ftp://opendata.dwd.de/climate_environment/CDC/Terms_of_use.pdf" ;
                :creation_date = "transformation to netCDF:2019-11-08 13:52:52 +0100" ;
    }
