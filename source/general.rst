.. SPDX-FileCopyrightText: 2017-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _general:

General specifications
======================

Provided here is general information that has nothing directly to do
with the netCDF format itself but should, however, be taken into
consideration.

File Names
----------

-  NetCDF files are designated with the extension .nc

-  The file name begins with a letter or a letter sequence. The letter
   sequence should allow users to draw conclusions about the type of
   data found in the file.

-  Customary sequences used thus far (e.g., *"ctd"* for CTD data, *"sf"* for
   ScanFish data) will be retained. Specification should still be
   designated for other devices.

-  When dealing with model data, either the model name with the version
   designation or the ExperimentID should be used in the first segment
   of the filename.

-  Spaces, umlauts, *"ß"*, special characters (except underscore *"_"*, dash
   *"-"* and periods) are not to be used in file names.

-  File names are encoded with the starting date (and possibly the end
   date) of the data contained in the file according to the format:
   ``YYYYMMDDHH``. The date provided complies with the scope of data. A
   yearly file usually only contains the numerical year listed as ``YYYY``.
   If appropriate, minutes can be added in the *"mm"* format and seconds
   can be added as *"SS"*. Unresolved timescales can be omitted.
   Additional information can be added after the date and after *"_"* or
   *"."*, such as variable designations, region, statistical processing
   abbreviations.

.. card:: Examples

    - ``ctd200904171324.nc`` for a CTD profile that began on April 17\ :sup:`th`,
      2009 at 13:24.
    - ``WAVE2016080312_gb.nc`` as wave model calculation output for 12:00 on
      03.08.2016 in the German Bight (gb)
    - ``cD3_0025_ERAi.1948-2015.T_2M.DB.mm.nc`` as a time series from COSMO-CLM
      simulation output for 2m air temperature reduced to the German Bight and
      *monthly mean*.

Naming Conventions
------------------

-  *Variables, dimensions and attribute names* begin with a letter and
   consist of letters, numbers and underscores.

-  Spaces, umlauts, ß, special characters (except underscore *"_"*) are
   not allowed.

-  Upper and lower case letters are relevant. See the third example below.

*Names* must not be differentiated through case sensitive designations
alone\ *.*

*Variable names* should adhere as much as possible to international
standards\ *.* For observable marine quantities, the BODC’s [6]_
P09 database is, for example, available.

.. card:: Examples

    - pressure, longitude, DRYT, air_pressure, FlugHoehe are valid
      *variable names*. Of the listed variable names, only DRYT (dry bulb
      temperature) is included in P09.
    - geogr. Breite, air pressure, Flughöhe, $velocity are all invalid
      *variable names*.
    - *Variable names* such as PRESSURE and pressure are not to be used
      together in the same netCDF file because they can only be differentiated
      in terms of case sensitivity. This also applies to *dimensions* and
      *attribute names*.

.. [6] http://vocab.nerc.ac.uk/collection/P09/current/
