.. SPDX-FileCopyrightText: 2017-2024 Helmholtz-Zentrum hereon GmbH
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _variable-attributes:

Variables und Associated Attributes
===================================

In principle, the variable name in the netCDF file can be freely chosen
(subject to the notes in Chapter 2.2), but the variable must be
described using the corresponding *attributes* (see table in 4.2). Many
communities have established standards for these names, such as the
often mentioned P09 database [8]_.

Both mandatory and optional attributes exist.

.. _variable-types:

Data Types
----------

Only the netCDF *data types* ``char``, ``byte``, ``short``, ``float``,
``real``, ``double`` are allowed.

.. _variable-mandatory:

Mandatory Attributes
--------------------

Information regarding the unit (units) as well as the definition
(``standard_name`` [9]_ and/or ``long_name``) *must* be provided for each
variable. In rare cases, it does not make sense to request a
``standard_name``. Only then is the *attribute* long_name mandatory and the
long_name should be constructed like a standard name.

.. card:: Example

    (dimensionless value, ``standard_name`` exists, ``long_name`` chosen freely)

    .. code-block::

        double Salinity(levels) ;
            Salinity:units = " 1e-3 " ;
            Salinity:standard_name = "sea_water_salinity" ;
            Salinity:long_name = "salinity from SeaBird CTD" ;

    If the fitting `Standard Name Modifier` does not exist in the table, it
    must be requested with cf-metadata similar to the procedure for the
    standard_name [9]_.

.. [8] http://seadatanet.maris2.nl/v_bodc_vocab_v2/browse.asp?order=conceptid&formname=search&screen=0&lib=p09&v1_0=conceptid%2Cpreflabel%2Caltlabel%2Cdefinition%2Cmodified&v2_0=0&v0_1=&v1_1=conceptid&v2_1=3&v0_2=&v1_2=preflabel&v2_2=3&v0_3=&v1_3=altlabel&v2_3=3&v0_4=&v1_4=modified&v2_4=9&v0_5=&v1_5=modified&v2_5=10&v1_6=&v2_6=&v1_7=&v2_7=

.. [9] If this does not already exist in the cf-conventions table, it should
  be requested. Beate Geyer can provide further information on standard names
  and how to apply for them and https://github.com/cf-convention/discuss/.



.. metadata_table:: data/variable_mandatory.yml

.. _variable-optional:

Optional Attributes
-------------------

Optional attributes should supplement the variable description in a
meaningful way.

.. metadata_table:: data/variable_optional.yml

.. card:: Example for using the attribute cell_methods

    .. code-block::

        dimensions:
                time = UNLIMITED ; // (744 currently)
                bnds = 2 ;
                rlon = 234 ;
                rlat = 228 ;
        variables:
            double time(time) ;
                time:standard_name = "time" ;
                time:long_name = "time" ;
                time:units = "seconds since 1948-01-01 00:00:00" ;
                time:bounds = "time_bnds" ;
            double time_bnds(time, bnds) ;
                time_bnds:long_name = "time bounds" ;
                time_bnds:units = "seconds since 1948-01-01 00:00:00" ;
            float TOT_PREC(time, rlat, rlon) ;
                TOT_PREC:standard_name = "precipitation_amount" ;
                TOT_PREC:long_name = "total precipitation amount" ;
                TOT_PREC:units = "kg m-2" ;
                TOT_PREC:cell_methods = "time: sum" ;

.. _variable-missing:

Handling Missing Data
---------------------

-  Missing values in the data set are indicated only by the ``_FillValue``
   *attribute*.

The *value* ``_FillValue`` can be set to ``NaN`` or ``NaN.f`` (with float). In
some instances, other values have been agreed upon for naming the missing
values due to technical reasons (1.0E20 is a frequently used standard).
Using an actual value has the advantage that in visualizations such as
ncWMS, the pixels with ``_FillValue`` are displayed transparently. Files
with a ``_FillValue`` = ``NaN`` cannot be used for ncWMS2 because ncWMS2 no
longer supports this (see Section :ref:`variable-types`).

.. _variable-coordinates:

Coordinates
-----------

Coordinates are the variables corresponding to a dimension. While the
correct information on the coordinates is immensely important, the
explanations provided here are general. Different examples on coordinate
information from various Hereon datasets, however, can be found in the
Appendix.

The dimensions assigned to degrees of longitude and latitude are to be
assigned indices from west to east and south to north in ascending
order.

.. _variable-latitude:

Latitude
~~~~~~~~

-  The variable name is ``lat`` and the format is float.
-  The *attribute* units is ``degrees_north`` [11]_.
-  The *attribute* standard_name is ``latitude``.
-  The *attribute* axis (optional) contains the *value* ``Y``.
-  For transformed grids (e.g., coordinate systems with rotated poles)
   only degrees is used as the units.

.. card:: Example

    .. code-block::

        dimensions:
            lon = 69 ;
            lat = 64 ;
        float lat(lat);
            lat:units = "degrees_north" ;
            lat:standard_name = "latitude" ;

.. [11] http://cfconventions.org/cf-conventions/cf-conventions.html#_independent_latitude_longitude_vertical_and_time_axes

.. _variable-longitude:

Longitude
~~~~~~~~~

- The variable name is ``lon`` and the format is float.
- The attribute units is ``degree_east`` [11]_
- The attribute standard_name is ``longitude``.
- The attribute axis (optional) contains the value ``X``.
- For transformed grids (e.g., coordinate systems with rotated poles)
  only degrees is used as the units.

.. card:: Example

    .. code-block::

        dimensions:
            lon = 69 ;
            lat = 64 ;
        float lon(lon);
            lon:units = "degrees_east" ;
            lon:standard_name = "longitude" ;

.. _variable-height:

Height and Depth (vertical coordinates)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- The positive attribute is mandatory as there is often disagreement
  concerning the direction of the vertical coordinate.
- The direction of ascending coordinate values must be provided with the
  positive attribute and the possible values up or down.

.. card:: Examples

    .. tab-set::

        .. tab-item:: General

            .. code-block::

                axis_name:units = "m" ;
                axis_name:positive = "down" ;

        .. tab-item:: Pressure

            .. code-block::

                double Pressure(levels) ;
                    Pressure:units = "dbar" ;
                    Pressure:standard_name = "sea_water_pressure" ;
                    Pressure:positive = "down" ;

            The coordinate origin is at the sea surface in this case; pressure increases with depth.

        .. tab-item:: Altimeter

            .. code-block::

                double Altimeter(levels) ;
                    Altimeter:units = "m" ;
                    Altimeter:positive = "up" ;
                    Altimeter: standard_name = "height_above_sea_floor" ;
                    Altimeter:long_name = "distance to bottom" ;

            The coordinate origin is at the seafloor in this case. The value
            increases closer to the sea surface.

        .. tab-item:: Depth

            .. code-block::

                double Depth(levels) ;
                Depth:units = "m" ;
                Depth:positive = "down" ;
                Depth:long_name = "water depth relative to sea surface" ;
                Depth:standard_name = "depth" ;

            The coordinate origin is at the sea surface in this case.

        .. tab-item:: 2D variable with depth

            .. code-block::

                double time(time) ;
                    time:standard_name = "time" ;
                    time:long_name = "time" ;
                    time:units = "seconds since 1979-01-01 00:00:00" ;
                float rlon(rlon) ;
                    rlon:standard_name = "grid_longitude" ;
                    rlon:long_name = "rotated longitude" ;
                    rlon:units = "degrees" ;
                float rlat(rlat) ;
                    rlat:standard_name = "grid_latitude" ;
                    rlat:long_name = "rotated latitude" ;
                    rlat:units = "degrees" ;
                float height_2m ;
                    height_2m:standard_name = "height" ;
                    height_2m:long_name = "height above the surface" ;
                    height_2m:units = "m" ;
                    height_2m:positive = "up" ;
                float T_2M(time, rlat, rlon) ;
                    T_2M:standard_name = "air_temperature" ;
                    T_2M:long_name = "2m temperature" ;
                    T_2M:units = "K" ;
                    T_2M:coordinates = "lon lat height_2m" ;

            Although the temperature is given at 2m above ground, there is no
            separate dimension for it.


.. _variable-time:

Time Coordinate
~~~~~~~~~~~~~~~

The ``units`` value is, for example:

- ``"days since YYYY-MM-DD HH:mm:SS"`` or

- suitable formulations [13]_

**There is no time zone indicator!** UTC is therefore automatically set as
the time coordinate.

It is most preferable to use double as the type, otherwise there is a
risk of unresolved seconds.

The ``calendar`` attribute should be set to ``proleptic-gregorian`` or
``standard``.

In simplified cases, such as calculations without a leap year or only
months with thirty days, the corresponding calendar attribute is taken
into account: ``noleap`` and/or ``360_day``.

.. card:: Example

    .. code-block::

        dimensions:
                time = UNLIMITED ; // (744 currently)
        variables:
            double time(time) ;
                time:standard_name = "time" ;
                time:long_name = "time" ;
                time:calendar = "proleptic_gregorian" ;

.. [13] Further information: http://cfconventions.org/cf-conventions/cf-conventions.html#time-coordinate


.. _variable-quality-flags:

Quality Flags
~~~~~~~~~~~~~

Quality flags are usually only used for observation data. The ultimate
goal in regard to quality is error estimation and indication. This could
then also apply to model output.

The following OceanSites regulations\ [14]_ are mandatory for data intended
for the Copernicus Marine Environment Monitoring Service and are managed
there as well.

If possible, observation data should always be denoted with quality
flags. For each data point, these quality flags indicate whether the
data point has undergone quality control and the specific result of this
quality control. The quality flag does not provide any information
whether the data point is close to reality.

In the netCDF file, the quality flag is an additional variable that has
the same name as the variable for which the quality specification
applies, but with an appended string "_QC" (OceanSITES) or "_qc" (CF
conventions, see the Flags chapter\ [15]_). “QC” stands for quality control
for the respective parameter. Example names are TIME_QC, POSITION_QC or
TEMP_QC. For these variables’ attributes, the following applies::

    conventions = "OceanSITES reference table 2" ;
    long_name = "quality flag" ;
    _FillValue = -128b ;
    valid_min = 0b ;
    valid_max = 9b ;
    flag_values = "0, 1, 2, 3, 4, 5, 7, 8, 9" ;
    flag_meanings = "no_qc_performed good_data probably_good_data
    bad_data_that_are_potentially_correctable bad_data value_changed
    not_used nominal_value interpolated_value missing_value" ;

The data variable refers to the Quality Flag by means of the
``ancillary_variable`` attribute.

.. card:: Example

    .. code-block::

        float PSAL(TIME, DEPTH) ;
            PSAL:units = "1e-3" ;
            PSAL:standard_name = "sea_water_practical_salinity" ;
            PSAL:_FillValue = -999.f ;
            PSAL:DM_indicator = "R" ;
            PSAL:ancillary_variables = "PSAL_QC" ;
            PSAL:valid_min = 0 ;
            PSAL:valid_max = 40 ;
            PSAL:long_name = "Practical salinity" ;
            PSAL:cell_methods = "TIME:point DEPTH:point LATITUDE:point LONGITUDE:point" ;
        byte PSAL_QC(TIME, DEPTH) ;
            PSAL_QC:flag_values = "0, 1, 2, 3, 4, 5, 7, 8, 9" ;
            PSAL_QC:flag_meanings = "no_qc_performed good_data probably_good_data bad_data_that_are_potentially_correctable bad_data value_changed not_used nominal_value interpolated_value missing_value" ;
            PSAL_QC:_FillValue = -128b ;
            PSAL_QC:long_name = "quality flag" ;
            PSAL_QC:conventions = "OceanSITES reference table 2" ;
            PSAL_QC:valid_min = 0b ;
            PSAL_QC:valid_max = 9b ;
        byte current_speed_qc(time, depth, lat, lon) ;
            current_speed_qc:long_name = "Current Speed Quality" ;
            current_speed_qc:standard_name = "status_flag" ;
            current_speed_qc:_FillValue = -128b ;
            current_speed_qc:valid_range = 0b, 2b ;
            current_speed_qc:flag_values = 0b, 1b, 2b ;
            current_speed_qc:flag_meanings = "quality_good sensor_nonfunctional outside_valid_range" ;

.. [14]
   https://repository.oceanbestpractices.org/handle/11329/874.2

.. [15]
   http://cfconventions.org/cf-conventions/cf-conventions.html#flags
