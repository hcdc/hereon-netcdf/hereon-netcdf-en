<!--
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# Contributing to the Binding Regulations for Storing Data as netCDF Files at Helmholtz-Zentrum hereon

First of all, thanks! :tada: We are always happy about any kind of
contributions to help us improving our netCDF regulations.

The source of this document is publicly available at the Gitlab of the
_Helmholtz-Zentrum Dresden Rossendorf_, HZDR. You can sign up at
https://codebase.helmholtz.cloud/ with your Github account or through the Helmholtz AAI
(if you are employed at a member insitution of the HGF).

## Report issues, give feedback or ask for support

If you encounter any issues or have questions for support, please create an
issue in the main repository of this documentation, here: 
https://codebase.helmholtz.cloud/hcdc/hereon-netcdf/hereon-netcdf-en/-/issues

If you want to reach the core development team directly, please do not hesitate
to send a mail to hcdc_support@hereon.de

## Contribute to the documentation

If you want to build and modify the documentation yourself, please have a look
into the _Installation_ section in the [README](README.md) of this repository.
