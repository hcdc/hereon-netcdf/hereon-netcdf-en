<!--
SPDX-FileCopyrightText: 2021-2024 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# hereon-netcdf-en

Binding Regulations for Storing Data as netCDF Files

## Building this documentation

If you want to build and modify this document locally, you need to install
sphinx.

Here are the steps that you need to build this documentation.

1. Install python on your computer, from source or via [miniconda](https://docs.anaconda.com/free/miniconda/)
2. clone this repository via `git clone https://codebase.helmholtz.cloud/hcdc/hereon-netcdf/hereon-netcdf-en.git`
3. `cd` into the directory that you cloned and run `pip install -r requirements.txt`
5. make your changes in the `*.rst` files, e.g. in [`index.rst`](source/index.rst)
6. run `make html` on linux or `make.bat html` on Windows (`make html` should work as well if it's available in the CLI) to generate the documentation.
7. open `build/html/index.html` in your local browser.

This document is written in restructured Text with [Sphinx](https://www.sphinx-doc.org/en/master/).
If you want to learn more about it, please have a look at the
examples in the [Sphinx documentation](https://www.sphinx-doc.org/en/master/).

Note: We highly recommend to use Visual Studio Code with the
[reStructuredText extension](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)
to enable a live preview of the documentation while writing.

## Extensions

This documentation is based upon sphinx and uses the following extension
modules:

- [myst-parser][myst-parser] for rendering markdown
- [sphinx-design][sphinx-design] for tabs, cards, etc.
- [sphinx-rtd-theme][sphinx-rtd-theme] as theme


[myst-parser]: https://myst-parser.readthedocs.io
[sphinx-design]: https://sphinx-design.readthedocs.io
[sphinx-rtd-theme]: https://sphinx-rtd-theme.readthedocs.io


## Technical note

This package has been generated from the template
https://codebase.helmholtz.cloud/hcdc/software-templates/sphinx-documentation-template.git.

See the template repository for instructions on how to update the skeleton for
this package.

## Contributing

Please see the [`CONTRIBUTING.md`](CONTRIBUTING.md) file about how to edit and
contribute to this documentation.

## License information

Copyright © 2024 Helmholtz-Zentrum hereon GmbH


Licensed under the EUPL-1.2

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the EUPL-1.2 license for more details.
